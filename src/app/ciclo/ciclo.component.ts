import { Component, OnInit, Input, OnDestroy, OnChanges } from "@angular/core";

@Component({
  selector: "app-ciclo",
  templateUrl: "./ciclo.component.html",
  styleUrls: ["./ciclo.component.css"]
})
export class CicloComponent implements OnInit, OnDestroy, OnChanges {
  @Input() initialValue: number;

  constructor() {}

  ngOnInit() {
    this.log("ngOnInit");
  }

  ngOnChanges() {
    this.log("ngOnChanges");
  }

  ngOnDestroy() {
    this.log("ngOnDestroy");
  }

  private log(hook: string) {
    console.log(hook);
  }
}
