import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-data-binding",
  templateUrl: "./data-binding.component.html",
  styleUrls: ["./data-binding.component.css"]
})
export class DataBindingComponent implements OnInit {
  url: string = "http://loiane.com";
  urlImage: string = "http://lorempixel.com/400/200/nature";

  valorAtual: string = "";
  valorSalvo: string = "";

  isMouseOver: boolean = false;

  frase: string = "abc";

  nomeCurso: string = "Angular";

  actualCounter: number = 15;

  onChangeCounter(event) {
    console.log(event.newCounterValue);
  }

  pessoa: object = {
    nome: "Thales Loreto",
    idade: 19
  };

  constructor() {}

  getValor() {
    return 1;
  }

  botaoClicado() {
    alert("fui clicado :3");
  }

  onKeyUp(event: KeyboardEvent) {
    this.valorAtual = (<HTMLInputElement>event.target).value;
  }

  salvarValor(valor: string) {
    this.valorSalvo = valor;
  }

  onMouseOverOut() {
    this.isMouseOver = !this.isMouseOver;
  }

  ngOnInit() {}
}
