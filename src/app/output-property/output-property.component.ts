import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";

@Component({
  selector: "contador",
  templateUrl: "./output-property.component.html",
  styleUrls: ["./output-property.component.css"]
})
export class OutputPropertyComponent implements OnInit {
  @Input() counter: number;

  @Output() changeCounter = new EventEmitter();

  @ViewChild("campoInput", null) campoInputValor: ElementRef;

  increment() {
    this.campoInputValor.nativeElement.value++;
    this.changeCounter.emit({ newCounterValue: this.counter });
  }

  decrement() {
    this.campoInputValor.nativeElement.value--;
    this.changeCounter.emit({ newCounterValue: this.counter });
  }

  constructor() {}

  ngOnInit() {}
}
